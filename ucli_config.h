#ifndef _UCLI_CONFIG_H
#define _UCLI_CONFIG_H

/* uncomment this if using as external/non-default configuration */
/*#define UCLI_HAS_CONFIG */

/* Use CR and LF as line ending */
#ifndef CONFIG_CRLF
#	define CONFIG_CRLF 0
#endif

/* Build default commands (help particulary) */
#ifndef CONFIG_BUILTIN_HELP
#	define CONFIG_BUILTIN_HELP 1
#endif

/* Max argument count */
#ifndef CONFIG_MAX_ARGC
#	define CONFIG_MAX_ARGC 4
#endif

/* Max command length */
#ifndef CONFIG_MAX_CMD_LENGTH
#	define CONFIG_MAX_CMD_LENGTH 16
#endif

/* Max total line length (command and arguments)*/
#ifndef CONFIG_MAX_LINE_LENGTH
#	define CONFIG_MAX_LINE_LENGTH 32
#endif


#endif /* _UCLI_CONFIG_H */
