#ifndef _UCLI_H
#define _UCLI_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#ifndef UCLI_HAS_CONFIG
#include "ucli_config.h"
#endif

typedef struct {
    char command[CONFIG_MAX_CMD_LENGTH];
    int (*handle)(int argc, char *argv[]);
} ucli_cmd_t;

int ucli_init(const ucli_cmd_t* commands, int count);

/**@brief         Feed the ucli
 * @param line    user input from console
 * @return        consumend character number */

int ucli_feed(char *line);
int ucli_feed_buffered(uint8_t* bytes, size_t len);

/**@brief         Print command result on the screen,
 *                by default, printf is wrapped
 * @param output  string to be displayed to the user
 */
extern void ucli_printf(const char * format, ...);

/**@brief         Echo typed characters on the screen,
 *                by default, printf is wrapped
 * @param output  string to be displayed to the user
 */
extern void ucli_echo(const char c);

#endif /* _UCLI_H */
