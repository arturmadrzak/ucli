#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include "ucli.h"


#if CONFIG_BUILTIN_HELP == 1
static int cmd_help(int argc, char *argv[]);
const ucli_cmd_t builtin_commands[] = {
    {"help", cmd_help},
};
#endif /* CONFIG_BUILTIN_HELP */


static struct {
    const ucli_cmd_t *commands;
    uint32_t count;

    char line[CONFIG_MAX_LINE_LENGTH];
    char *tail;
} ucli;

static const ucli_cmd_t *match(char *cmd, const ucli_cmd_t *list, uint32_t count)
{
    uint32_t i;
    for(i = 0; i < count; i++) {
        if (strcmp(cmd, list[i].command) == 0
                && strlen(cmd) == strlen(list[i].command))
        {
            return &list[i];
        }
    }
    return NULL;

}

static const ucli_cmd_t *match_command(char *cmd)
{
    const ucli_cmd_t *found = NULL;

#if CONFIG_BUILTIN_HELP == 1
    found = match(cmd, builtin_commands, sizeof(builtin_commands)/sizeof(ucli_cmd_t));
#endif /* CONFIG_BUILTIN_HELP */

    if (!found)
        found = match(cmd, ucli.commands, ucli.count);

    return found;
}
static void reset_buffered()
{
    memset(ucli.line, 0, sizeof(ucli.line));
    ucli.tail = ucli.line;
    ucli_printf("ucli> ");
}

__attribute__((weak))
void ucli_printf(const char * format, ...) 
{
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
    fflush(stdout);
}

__attribute__((weak))
void ucli_echo(const char c) 
{
    ucli_printf("%c", c);
}


int ucli_init(const ucli_cmd_t* cmds, int count)
{
    int i;

    if (!cmds || !count)
        return -1;

    for(i = 0; i < count; i++) {
        if (!cmds[i].handle || !cmds[i].command || strlen(cmds[i].command) <= 0)
            return -1;
    }

    ucli.commands = cmds;
    ucli.count = count;
    reset_buffered();
    return 0;
}

int ucli_feed(char* line)
{
    int length;
    char* argv[CONFIG_MAX_ARGC];
    int argc = 0;
    if (!ucli.commands || !line)
        return -1;

    length = strlen(line);
    if (length > CONFIG_MAX_LINE_LENGTH || length == 0)
        return -1;

    while((argv[argc] = strsep(&line," ")) != NULL)
    {
        if (*argv[argc] == '\0')
            continue;
        if (++argc > CONFIG_MAX_ARGC)
            return -1;
    }

    const ucli_cmd_t *cmd = match_command(argv[0]);
    if (cmd) 
    {
        return cmd->handle(argc, argv);
    }    
    /* command not found */
    return -1;
}


int ucli_feed_buffered(uint8_t* bytes, size_t len)
{
    uint8_t *end;
    size_t consumed = 0;

    if (!ucli.commands)
        return -1;

    for(end = bytes + len; bytes < end; bytes++)
    {
        if (isprint((int)*bytes))
        {
            if (ucli.tail > (ucli.line + sizeof(ucli.line) - 1))
                return -1;

            ucli_echo(*bytes);
            *ucli.tail = *bytes;
            consumed++;
            ucli.tail++;
        } 
        else switch(*bytes)
        {
            case '\r':
            case '\n':
                {
                    *ucli.tail = 0;
                    consumed++;
                    ucli_echo('\r');
                    ucli_echo('\n');
                    ucli_feed(ucli.line);
                    reset_buffered();
                    return consumed;
                }
                break;
            case '\b':
                {
                    if (ucli.tail > ucli.line) 
                    {
                        *ucli.tail = 0;
                        ucli.tail--;
                        ucli_echo('\b');
                        ucli_echo(' ');
                        ucli_echo('\b');
                    }
                    consumed++;
                }
                break;
            case 68:
                {
                    // key left
                }
                break;
            case 67:
                {
                    // key right
                }
        }
    }
    return consumed;
}

#if CONFIG_BUILTIN_HELP == 1
static int cmd_help(int argc, char *argv[])
{
    int i;
    ucli_printf("Commands: \r\n");

    for(i = 0; i < ucli.count; i++) {
        ucli_printf("\t%s\r\n",ucli.commands[i].command);
    }
    ucli_printf("Type [command] to get more help\r\n");
    ucli_printf("\r\nPowered by ucli\r\n");
    return 0;
}

#endif /* CONFIG_BUILTIN_HELP */
