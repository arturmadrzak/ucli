#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>
#include "ucli.h"

static int test_handle_stub(int argc, char *argv[])
{
    return (int)mock(argc, argv);
}

static ucli_cmd_t commands[] = {
    {"test", test_handle_stub}
};

Describe(ucli_feed);

BeforeEach(ucli_feed) 
{
    cgreen_mocks_are(loose_mocks);
}

AfterEach(ucli_feed) {}

Ensure(ucli_feed, fails_if_not_initialized)
{
    char line[] = "test arg1 arg2";
    assert_that(ucli_feed(line), is_not_equal_to(0));
}

Ensure(ucli_feed, no_segfault_for_null)
{
    ucli_init(commands,1);
    assert_that(ucli_feed(NULL),is_not_equal_to(0));
}

Ensure(ucli_feed, fails_if_line_too_long)
{
    char line[CONFIG_MAX_LINE_LENGTH + 1];
    memset(line, 'A', sizeof(line));

    ucli_init(commands,1);
    assert_that(ucli_feed(line),is_not_equal_to(0));
}

Ensure(ucli_feed, fails_if_too_many_tokens)
{
#if CONFIG_MAX_ARGC != 4
#error "Test is designed for argc = 4"
#endif
    char line[] = "test one two three four";
    ucli_init(commands,1);
    assert_that(ucli_feed(line), is_not_equal_to(0));
}

Ensure(ucli_feed, no_fail_for_spaces_in_a_row)
{
    char line[] = "test  arg1     arg2";
    expect(test_handle_stub, will_return(0));
    ucli_init(commands,1);
    assert_that(ucli_feed(line), is_equal_to(0));
}

Ensure(ucli_feed, fail_if_command_not_found)
{
    char line[] = "test2 arg1 arg2";
    ucli_init(commands,1);
    assert_that(ucli_feed(line), is_not_equal_to(0));
}

Ensure(ucli_feed, argc_is_correct)
{
    char line[] = "test arg1 arg2";
    expect(test_handle_stub, when(argc, is_equal_to(3)),will_return(0));
    ucli_init(commands,1);
    assert_that(ucli_feed(line), is_equal_to(0));
}

Ensure(ucli_feed, arg_values_are_correct)
{
    char line[] = "test arg1 arg2";
    char *expect[] = {&line[0], &line[5], &line[10]};
    expect(test_handle_stub, 
            when(argv, is_equal_to_contents_of(expect,sizeof(expect))),
            will_return(0));
    ucli_init(commands,1);
    assert_that(ucli_feed(line), is_equal_to(0));
}

Ensure(ucli_feed, passthrough_command_return_code)
{
    char line[] = "test arg1 arg2";
    expect(test_handle_stub, will_return(345));
    ucli_init(commands,1);
    assert_that(ucli_feed(line), is_equal_to(345));
}

Ensure(ucli_feed, no_fail_if_no_arg)
{
    char line[] = "test";
    expect(test_handle_stub, when(argc, is_equal_to(1)), will_return(0));
    ucli_init(commands,1);
    assert_that(ucli_feed(line), is_equal_to(0));
}

Ensure(ucli_feed, fail_if_empty_string)
{
    char line[] = "\0";
    ucli_init(commands,1);
    assert_that(ucli_feed(line), is_not_equal_to(0));
}
