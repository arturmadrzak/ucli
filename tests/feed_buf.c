#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>
#include "ucli.h"

static int test_handle_stub(int argc, char *argv[])
{
    return (int)mock(argc, argv);
}

static ucli_cmd_t commands[] = {
    {"test", test_handle_stub}
};


Describe(ucli_feed_buffered);

BeforeEach(ucli_feed_buffered) 
{
    cgreen_mocks_are(loose_mocks);
    ucli_init(commands, 1);
}

AfterEach(ucli_feed_buffered) {}

Ensure(ucli_feed_buffered, fails_if_not_initialized)
{
    char line[] = "test arg1 arg2";
    assert_that(ucli_feed_buffered(line, sizeof(line)), is_not_equal_to(0));
}

Ensure(ucli_feed_buffered, execute_command_line)
{
    expect(test_handle_stub, will_return(0));
    assert_that(ucli_feed_buffered((uint8_t*)"test 0123456789 0123456789 01234\r\n", 34), is_equal_to(33));
}

Ensure(ucli_feed_buffered, fails_if_line_too_long)
{
    assert_that(ucli_feed_buffered((uint8_t*)"test 0123456789 0123456789 012345\r\n", 35), is_less_than(0));
}


Ensure(ucli_feed_buffered, consume_preceding_lr_or_cf)
{
    assert_that(ucli_feed_buffered((uint8_t*)"\n\ntest\r", 7), is_equal_to(1));
    assert_that(ucli_feed_buffered((uint8_t*)"\r\rtest\n", 7), is_equal_to(1));
}

Ensure(ucli_feed_buffered, consume_chars_if_no_lr_or_cf)
{
    assert_that(ucli_feed_buffered((uint8_t*)"testx", 5), is_equal_to(5));
}

Ensure(ucli_feed_buffered, remove_char_if_backspace_typed)
{
    expect(test_handle_stub, will_return(0));
    assert_that(ucli_feed_buffered((uint8_t*)"testx", 5), is_equal_to(5));
    assert_that(ucli_feed_buffered((uint8_t*)"\b\r\n", 3), is_equal_to(2));
}


Ensure(ucli_feed_buffered, do_not_segfault_if_backspace_pressed_on_empty_buffer)
{
    assert_that(ucli_feed_buffered((uint8_t*)"te", 2), is_equal_to(2));
    assert_that(ucli_feed_buffered((uint8_t*)"\b\b\b\b\b", 5), is_equal_to(5));
}

Ensure(ucli_feed_buffered, echo_input_chars)
{
    const char given[] = "echo";
    cgreen_mocks_are(strict_mocks);
    expect(ucli_echo, when(c, is_equal_to('e')));
    expect(ucli_echo, when(c, is_equal_to('c')));
    expect(ucli_echo, when(c, is_equal_to('h')));
    expect(ucli_echo, when(c, is_equal_to('o')));
    assert_that(ucli_feed_buffered((uint8_t*)given, strlen(given)), is_equal_to(strlen(given)));
}


Ensure(ucli_feed_buffered, backspace_destroy_char)
{
    const char given[] = "echo\ba";
    cgreen_mocks_are(strict_mocks);
    expect(ucli_echo, when(c, is_equal_to('e')));
    expect(ucli_echo, when(c, is_equal_to('c')));
    expect(ucli_echo, when(c, is_equal_to('h')));
    expect(ucli_echo, when(c, is_equal_to('o')));
    expect(ucli_echo, when(c, is_equal_to('\b')));
    expect(ucli_echo, when(c, is_equal_to(' ')));
    expect(ucli_echo, when(c, is_equal_to('\b')));
    expect(ucli_echo, when(c, is_equal_to('a')));
    assert_that(ucli_feed_buffered((uint8_t*)given, strlen(given)), is_equal_to(strlen(given)));

}
