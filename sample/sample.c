#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <readline/readline.h>
#include "ucli.h"

static bool quit = false;

/* Echo is enabled by default. Suppress it.*/
void ucli_echo(const char c) {}

static int cmd_exit(int argc, char *argv[])
{
    ucli_printf("\r\n");
    quit = true;
    return 0;
}

static const ucli_cmd_t commands[] = {
    {"exit", cmd_exit}
};

void signal_handler(int signum)
{
    quit = true;
}

int main(int argc, char **argv) 
{
    char *line = NULL;

    signal(SIGINT, signal_handler); 
    signal(SIGTERM, signal_handler); 

    ucli_init(commands, 1);

    while(!quit)
    {
        line = readline(NULL);
        ucli_feed_buffered((uint8_t*)line, strlen(line)+1);
        ucli_feed_buffered((uint8_t*)"\r", 1);
        if (line) 
            free(line);
    }
    return 0;
}

