[![pipeline status](https://gitlab.com/arturmadrzak/ucli/badges/master/pipeline.svg)](https://gitlab.com/arturmadrzak/ucli/commits/master)

# ucli - Micro Command Line Interface
Serial port (USART, UART, semi-hosting, etc.) is a standard these days and available 
on all Cortex-M devices and it's natural to build some kind of interface for developer/user 
to interact with running application. It's not neccessarly intended to use
in production, but it's inarguably needed during development process. This library
provides simple shell behaviour and "main-like" callbacks for developer. 
Library uses standard system calls from libc (newlibc),
so it's portable and can work with no code using semi-hosting or requires minimum
conding if used with retargeting stdio.

# Build
There is no special requirements to build both library and sample application. 
Typing ```make``` will build library, sample app and unit-test. If no ```cgreen``` 
framework available, then
```
make lib
```
or 
```
make sample
```
can be used to build static library and sample application respectively. 

# Test
[cgreen](https://github.com/cgreen-devs/cgreen) is required.
Running unit test is simple like a type and invoke: 

```
make test
```
Unit test are invoked by default, so there is no need to run test separetly.


# Integration
Both ```ucli_printf()``` and ```ucli_echo()``` are **weak** functions and by default mapped
to system ```printf()```. If no I/O retarget is used for **stdlib** then at least 
```ucli_printf()``` custom implementation is needed, something like:
```
void ucli_printf(const char *format, ...)
{
  va_start(args, format);
  uint8_t buf[16];
  int len = vsnprintf(buf, sizeof(buf), format, args);
  va_end(args);
  while(len > 0) {
      wait_for_uart();
      UART1->DATA = buf++;
      len--;
  }
}
```
Other way round is application specific and ucli needs to be feed by ```ucli_feed``` 
or ```ucli_feed_buffered```, depending if line reader is availabe or not.

# Disable echo
To disable echo (check sample app), just override ```ucli_echo()``` somewhere
in main.c:
```
void ucli_echo(const char c) {};
```

# Example 
Sample Linux implementation and usage of library can be found in ```sample``` dir.

# License
The **ucli** is available under the MIT License.