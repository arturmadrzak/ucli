SRCS = $(wildcard *.c)
OBJS = $(SRCS:.c=.o)
DEPS = $(SRCS:.c=.d)

CFLAGS = -Wall -fPIC -MMD

.PHONY: all lib test sample clean

all: lib sample test

lib: libucli.a

test: lib
	make -C tests

sample: lib
	make -C sample 

libucli.a: $(OBJS)
	$(AR) csr $@ $^

clean:
	make -C sample clean
	make -C tests clean
	rm -rf $(TESTDIR)
	rm -f $(OBJS)
	rm -f $(DEPS)
	rm -f libucli.a

include $(wildcard *.d)
