#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>
#include <string.h>
#include "ucli.h"

Describe(ucli_init);

BeforeEach(ucli_init) 
{
    cgreen_mocks_are(loose_mocks);
}

AfterEach(ucli_init) {}

Ensure(ucli_init, no_segfault_for_null)
{
    assert_that(ucli_init(NULL, 4),is_not_equal_to(0));
}

Ensure(ucli_init, fail_on_empty_command_list)
{
    ucli_cmd_t cmd;
    assert_that(ucli_init(&cmd, 0),is_not_equal_to(0));
}

Ensure(ucli_init, fail_if_null_command_handler)
{
    ucli_cmd_t cmd = {
        .command = "cmd",
        .handle = NULL
    };
    assert_that(ucli_init(&cmd, 1),is_not_equal_to(0));
}

Ensure(ucli_init, fail_if_empty_command)
{
    ucli_cmd_t cmd = {
        .command = "",
        .handle = (void*)1
    };
    assert_that(ucli_init(&cmd, 1),is_not_equal_to(0));
}

Ensure(ucli_init, success_if_correct)
{
    ucli_cmd_t cmds[] = {
        {"ls", (void*)1},
        {"cat", (void*)1}
    };
    assert_that(ucli_init(cmds, 2),is_equal_to(0));
}
